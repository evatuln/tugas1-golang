package main

import (
	"fmt"
)

func main() {

	var p, l, kll, L int32

	fmt.Print("Input Panjang : ")
	fmt.Scanln(&p)
	fmt.Print("Input Lebar : ")
	fmt.Scanln(&l)

	kll = 2 * (p + l)
	L = p * l

	fmt.Println("Keliling Persegi Panjang = ", kll)
	fmt.Println("Luas Persegi Panjang = ", L)

}
